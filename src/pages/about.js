import React, { Component } from 'react'

export default class About extends Component {
  render() {
	return (
	  <div>
		<h1>About Us</h1>
		<p>Tote bag brunch banjo vegan, roof party distillery +1 typewriter. Beard bespoke vice cred subway tile next level kale chips taxidermy lyft. Dreamcatcher food truck synth vice try-hard prism raclette, health goth street art franzen edison bulb activated charcoal hexagon waistcoat. Tofu cardigan bespoke, humblebrag meh twee letterpress four dollar toast deep v street art selvage pop-up hot chicken aesthetic skateboard. Meh taxidermy cold-pressed tousled hot chicken lyft. Actually iceland listicle prism.</p>
	  </div>
	)
  }
}
