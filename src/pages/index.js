import React from 'react';
import Link from 'gatsby-link';
import PostList from '../components/posts/post-list';

const IndexPage = ( { data } ) => (
	<div>
		<h2>Hipster Posts</h2>
		{ data.allMarkdownRemark.edges.map( ( { node } ) => {
			return <PostList key={ node.id } post={ node } />;
		} ) }
	</div>
);

export default IndexPage;

export const query = graphql`
	query SiteMeta {
		site {
			siteMetadata {
			title
			description
			}
		}
		allMarkdownRemark( sort: {
			fields: [ frontmatter___date ],
			order: DESC
		} ) {
			edges {
				node {
					id
					frontmatter {
						title
						date(formatString: "MMMM DD, YYYY")
					}
					html
					excerpt( pruneLength: 280 )
					timeToRead
					fields {
						slug
					}
				}
			}
		}
	}
`;

