import React, { Component } from 'react';

class Single extends Component {
	render() {
		const { data } = this.props;

		return (
			<div>
				<p>{data.markdownRemark.frontmatter.date}</p>
				<h1>{data.markdownRemark.frontmatter.title}</h1>
				<div
					dangerouslySetInnerHTML={{
						__html: data.markdownRemark.html,
					}}
				/>
			</div>
		);
	}
}

export default Single;

export const query = graphql`
	query SinglePostQuery($slug: String!) {
		markdownRemark(fields: { slug: { eq: $slug } }) {
			excerpt
			html
			frontmatter {
				title
				date(formatString: "MMMM DD, YYYY")
			}
		}
	}
`;
