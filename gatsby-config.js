module.exports = {
	siteMetadata: {
		title: 'Level Up Blog',
		description: 'A new Gatsby blog'
	},
	plugins: [
		'gatsby-plugin-react-helmet',
		'gatsby-plugin-styled-components',
		{
			resolve: 'gatsby-source-filesystem',
			options: {
				name: 'src',
				path: `${ __dirname }/src/`
			}
		},
		{
			resolve: 'gatsby-source-filesystem',
			options: {
				name: 'img',
				path: `${ __dirname }/src/images/`
			}
		},
		{
			resolve: 'gatsby-transformer-remark',
			options: {
				excerpt_separator: `<!-- more -->`
			}
		},
		// 'gatsby-transformer-remark',
		'gatsby-transformer-sharp',
		'gatsby-plugin-sharp'
	],
}
